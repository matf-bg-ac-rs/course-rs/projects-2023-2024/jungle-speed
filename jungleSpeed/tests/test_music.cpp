#include "catch2/catch_all.hpp"
#include <iostream>
#include "../src/gui/music.h"

TEST_CASE("Testing Music")
{
    SECTION("Testing musicStatus when music is off")
    {
        bool isSoundOn = false;
        int value = 50;

        Music music(isSoundOn, value);

        REQUIRE(music.musicStatus() == false);
    }

    SECTION("Testing musicStatus when music is on")
    {
        bool isSoundOn = true;
        int value = 50;

        Music music(isSoundOn, value);

        REQUIRE(music.musicStatus() == true);
    }

    SECTION("Testing playMusic when music is off")
    {
        bool isSoundOn = false;
        int value = 60;

        Music music(isSoundOn, value);
        music.playMusic();

        REQUIRE(music.musicStatus() == true);
    }

    SECTION("Testing stopMusic when music is on")
    {

        bool isSoundOn = true;
        int value = 40;

        Music music(isSoundOn, value);
        music.stopMusic();

        REQUIRE(music.musicStatus() == false);
    }

    SECTION("Testing scaleVolume")
    {
        bool isSoundOn = true;
        int value = 50;

        Music music(isSoundOn, value);

        float expectedVolume = 0.5;
        float receivedVolume = music.scaleVolume(value);

        REQUIRE(expectedVolume == receivedVolume);
    }


}


