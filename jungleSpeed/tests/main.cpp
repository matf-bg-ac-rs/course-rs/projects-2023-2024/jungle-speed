#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"
#include <iostream>
#include <QApplication>
#include "test_cards.cpp"
#include "test_music.cpp"
#include "test_player.cpp"
#include "test_map_players.cpp"
#include "test_gameserver.cpp"

int main(int argc, char* argv[])
{

    QApplication app(argc, argv);
    return Catch::Session().run(argc, argv);
}
