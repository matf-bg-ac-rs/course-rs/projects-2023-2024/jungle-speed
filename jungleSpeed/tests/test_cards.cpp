#include "catch2/catch_all.hpp"
#include <iostream>
#define private public
#include "../src/cards/Card.h"
#include "../src/cards/Color.h"
#include "../src/cards/RegularCard.h"
#include "../src/cards/SpecialCard.h"
#include "../src/cards/SpecialType.h"
#include "../src/server/gameserver.h"
#include "../src/server/server.h"
#include "../src/player/player.h"

TEST_CASE("Constructing cards", "[constructor]")
{

    SECTION("Regular Card with arguments.")
    {

        // Arrange
        auto regularCard = new RegularCard(Color::Red, Shape::CB);

        auto expected_color = Color::Red;
        auto expected_shape = Shape::CB;

        // Act
        auto color = regularCard->getColor();
        auto shape = regularCard->getShape();

        // Assert

        REQUIRE(expected_color == color);
        REQUIRE(expected_shape == shape);
    }

    SECTION("Regular Card with no arguments given.")
    {
        // Arrange
        auto regularCard = new RegularCard();

        auto expected_color = Color::Blue;
        auto expected_shape = Shape::CG;

        // Act
        auto color = regularCard->getColor();
        auto shape = regularCard->getShape();

        // Assert

        REQUIRE(expected_color == color);
        REQUIRE(expected_shape == shape);
    }

    SECTION("Special card with no arguments given.")
    {
        // Arrange
        auto specialCard = new SpecialCard();

        auto expected_result = SpecialType::colored;

        // Act
        auto type = specialCard->getSpecialType();

        // Assert
        REQUIRE(expected_result == type);
    }

    SECTION("Special card with arguments.")
    {
        // Arrange
        auto specialCard = new SpecialCard(SpecialType::inwardFacing);

        auto expected_result = SpecialType::inwardFacing;

        // Act
        auto type = specialCard->getSpecialType();

        // Assert
        REQUIRE(expected_result == type);
    }
}

TEST_CASE("Some server functions for deck.", "[function]")
{
        SECTION("Deck initialization")
        {
            // Arrange

            auto server = new gameServer();
            auto expected_result = 78;
            QVector<Card*> deck = {};

            // Act

            server->initalizeDeck(deck);

            // Assert

            REQUIRE(expected_result == deck.size());
        }

        SECTION("Deck distribution")
        {
            // Arrange

            QVector<Player*> players = {};
            auto player_1 = new Player("1");
            auto player_2 = new Player("2");
            players.emplace_back(player_1);
            players.emplace_back(player_2);

            auto server = new gameServer(nullptr, players);
            server->setPartySize(2);
            QVector<Card*> deck = {};

            auto expected_res = 39;

            // Act

            server->initalizeDeck(deck);
            server->setDeck(deck);
            server->distribute();

            // Assert

            REQUIRE(expected_res == server->m_players[0]->getDeck().size());
        }
 }

TEST_CASE("Compare cards", "[compare]")
{

        SECTION("Regular cards comparison by color") {
            Card *card1 = new RegularCard(Color::Red, Shape::XO);
            Card *card2 = new RegularCard(Color::Red, Shape::CB);
            Card *card3 = new  RegularCard(Color::Blue, Shape::XO);

            REQUIRE(card1->compareCards(card2, true));
            REQUIRE_FALSE(card1->compareCards(card3, true));
            delete card1,card2,card3;
        }

        SECTION("Regular cards comparison by shape") {

            Card *card1 = new RegularCard(Color::Blue, Shape::XO);
            Card *card2 = new RegularCard(Color::Red, Shape::XO);
            Card *card3 = new RegularCard(Color::Red, Shape::CB);

            REQUIRE(card1->compareCards(card2, false));
            REQUIRE_FALSE(card1->compareCards(card3, false));
            delete card1,card2,card3;
        }

        SECTION("Non-Regular cards comparison") {
            Card *card1 = new  SpecialCard(SpecialType::colored);
            Card *card2 = new  RegularCard(Color::Red, Shape::CB);
            Card *card3 = new  SpecialCard(SpecialType::colored);

            REQUIRE_FALSE(card1->compareCards(card2, true));
            REQUIRE_FALSE(card1->compareCards(card3, false));
            delete card1,card2,card3;
        }

}


TEST_CASE("Testing for winners", "[winner]")
{

        SECTION("Winner existing"){
            auto game = new gameServer();
            auto player1 = new Player("Nikola");
            auto player2 = new Player("Lazar");
            game->m_players.push_back(player1);
            game->m_players.push_back(player2);
            game->m_partySize = 2;

            game->initalizeDeck(game->m_deck);
            game->distribute();

            game->m_players[0]->m_discard.clear();
            game->m_players[0]->m_deck.clear();

            REQUIRE(game->checkWinner());

        }

        SECTION("Winner doesn't exist"){

            auto game = new gameServer();
            auto player1 = new Player("Nikola");
            auto player2 = new Player("Lazar");
            game->m_players.push_back(player1);
            game->m_players.push_back(player2);
            game->m_partySize = 2;


            game->initalizeDeck(game->m_deck);
            game->distribute();


            REQUIRE_FALSE(game->checkWinner());
        }
}
