#!/bin/bash
username=$(whoami)
rm -rf build

conan install . --output-folder=build --build=missing --settings=build_type=Debug
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_PREFIX_PATH=/home/${username}/Qt/6.6.0/gcc_64 -G "Unix Makefiles"
cmake --build .
