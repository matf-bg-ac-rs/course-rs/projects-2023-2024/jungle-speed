#include "catch2/catch_all.hpp"
#define private public
#include "../src/server/gameserver.h"
#include "../src/player/player.h"
#include <QJsonObject>

TEST_CASE("Player ready")
{

    SECTION("Player ready size less than party size"){


        // Arrange
        auto game = new gameServer();
        auto player1 = new Player("Player1");
        player1->setId(1);
        auto player2 = new Player("Player2");
        player2->setId(2);
        auto player3 = new Player("Player3");
        player3->setId(3);
        game->m_players.emplace_back(player1);
        game->m_players.emplace_back(player2);
        game->m_players.emplace_back(player3);
        game->m_partySize = 3;
        auto expected_result = game->m_readySize+1;
        bool signalEmitted = false;

        QObject::connect(game, &gameServer::initialize, [&]() {
            signalEmitted = true;
        });

        // Act
        game->onPlayerReady();
        auto size = game->m_readySize;

        // Assert
        REQUIRE(expected_result==size);
        REQUIRE(!signalEmitted);

    }

    SECTION("Player ready size equal to party size"){


        // Arrange
        auto game = new gameServer();
        auto player1 = new Player("Player1");
        player1->setId(1);
        auto player2 = new Player("Player2");
        player2->setId(2);
        auto player3 = new Player("Player3");
        player3->setId(3);
        game->m_players.emplace_back(player1);
        game->m_players.emplace_back(player2);
        game->m_players.emplace_back(player3);
        game->m_partySize = 3;
        game->m_readySize = 2;
        auto expected_result = game->m_readySize+1;
        bool signalEmitted = false;

        QObject::connect(game, &gameServer::initialize, [&]() {
            signalEmitted = true;
        });

        // Act
        game->onPlayerReady();
        auto size = game->m_readySize;

        // Assert
        REQUIRE(expected_result==size);
        REQUIRE(signalEmitted);

    }

}

TEST_CASE("Player added")
{

    SECTION("Verify that player was added and signal is emited with right argument"){


        // Arrange
        auto game = new gameServer();
        auto player1 = new Player("Player1");
        player1->setId(1);
        auto player2 = new Player("Player2");
        player2->setId(2);
        game->m_players.emplace_back(player1);
        game->m_players.emplace_back(player2);

        auto expected_result = game->m_players.size()+1;
        bool signalEmitted = false;
        int signalArgument;
        auto signal_expected_result = game->m_players.size()+1;

        QObject::connect(game, &gameServer::lobbySizeChanged, [&](int x) {
            signalEmitted = true;
            signalArgument = x;
        });

        // Act
        game->playerAdded("Player3",3);
        auto size = game->m_players.size();

        // Assert
        REQUIRE(expected_result==size);
        REQUIRE(signalEmitted);
        REQUIRE(signalArgument==size);
    }

}

TEST_CASE("Losers rearrange")
{

    SECTION("Verify sizes of discard and deck piles for all players"){


        // Arrange
        QVector<Player*> players = {};
        auto player_1 = new Player("1");
        auto player_2 = new Player("2");
        players.emplace_back(player_1);
        players.emplace_back(player_2);

        auto game = new gameServer(nullptr, players);
        game->setPartySize(2);
        QVector<Card*> deck = {};
        game->initalizeDeck(deck);
        game->setDeck(deck);
        game->distribute();
        for(int i =0;i<5;i++)
        {
            player_1->openNextCard();
            player_2->openNextCard();
            player_1->openNextCard();
        }
        auto discard1 = player_1->getDiscard().size();
        auto discard2 = player_2->getDiscard().size();
        auto deck1 = player_1->getDeck().size();
        auto deck2 = player_2->getDeck().size();
        auto expected_discard1 = 0;
        auto expected_discard2 = 0;
        auto expected_deck1 = deck1+discard1;
        auto expected_deck2 = deck2+discard2;

        // Act
        game->losersRearrange(players);
        auto discard1_after = player_1->getDiscard().size();
        auto discard2_after = player_2->getDiscard().size();
        auto deck1_after = player_1->getDeck().size();
        auto deck2_after = player_2->getDeck().size();

        // Assert
        REQUIRE(expected_discard1==discard1_after);
        REQUIRE(expected_discard2==discard2_after);
        REQUIRE(expected_deck1==deck1_after);
        REQUIRE(expected_deck2==deck2_after);
        REQUIRE(discard1!=0);
        REQUIRE(discard2!=0);
        REQUIRE(deck1_after!=0);
        REQUIRE(deck2_after!=0);
    }

}

TEST_CASE("Totem grabbed")
{

    SECTION("Verify that m_totem and m_duel change and signal is emited"){


        // Arrange
        auto game = new gameServer();
        auto player1 = new Player("Player1");
        player1->setId(1);
        auto player2 = new Player("Player2");
        player2->setId(2);
        game->m_players.emplace_back(player1);
        game->m_players.emplace_back(player2);

        auto expected_m_totem = true;
        auto expected_m_duel = player1;
        bool signalEmitted = false;

        QObject::connect(game, &gameServer::interactionComplete, [&]() {
            signalEmitted = true;
        });

        QJsonObject doc;
        doc["id"] = QString::number(player1->getId());

        // Act
        game->onTotemGrabbed(doc);
        auto totem = game->m_totem;
        auto duel = game->m_duel;

        // Assert
        REQUIRE(expected_m_duel==duel);
        REQUIRE(expected_m_totem==totem);
        REQUIRE(signalEmitted);
    }

}

TEST_CASE("Removing player")
{

    SECTION("Player with id existing"){


        // Arrange
        auto game = new gameServer();
        auto player1 = new Player("Player1");
        player1->setId(1);
        auto player2 = new Player("Player2");
        player2->setId(2);
        auto player3 = new Player("Player3");
        player3->setId(3);
        game->m_players.emplace_back(player1);
        game->m_players.emplace_back(player2);
        game->m_players.emplace_back(player3);
        game->m_partySize = 3;
        auto id = 2;
        auto expected_result = game->m_partySize-1;

        // Act
        game->removePlayer(id);
        auto size = game->m_partySize;

        // Assert
        REQUIRE(expected_result==size);

    }

    SECTION("Player with id not existing"){


        // Arrange
        auto game = new gameServer();
        auto player1 = new Player("Player1");
        player1->setId(1);
        game->m_players.emplace_back(player1);
        game->m_partySize = 1;
        auto id = 2;
        auto expected_result = game->m_partySize;

        // Act
        game->removePlayer(id);
        auto size = game->m_partySize;

        // Assert
        REQUIRE(expected_result==size);

    }
}
