#include "catch2/catch_all.hpp"
#include <iostream>
#include "../src/gui/Functions.h"

TEST_CASE("Map players")
{

    SECTION("One player")
    {
        qsizetype n = 1;
        unsigned id = 1;
        QVector<unsigned> expected = {1};

        QVector<unsigned> rotated = mapPlayers(n, id);
        bool same = expected == rotated;

        REQUIRE(same == true);
    }

    SECTION("Two players. id=1")
    {
        qsizetype n = 2;
        unsigned id = 1;
        QVector<unsigned> expected = {1,2};

        QVector<unsigned> rotated = mapPlayers(n, id);
        bool same = expected == rotated;

        REQUIRE(same == true);
    }

    SECTION("Two players. id=2")
    {
        qsizetype n = 2;
        unsigned id = 2;
        QVector<unsigned> expected = {2,1};

        QVector<unsigned> rotated = mapPlayers(n, id);
        bool same = expected == rotated;

        REQUIRE(same == true);
    }

    SECTION("Four players. id=1")
    {
        qsizetype n = 4;
        unsigned id = 1;
        QVector<unsigned> expected = {1,2,3,4};

        QVector<unsigned> rotated = mapPlayers(n, id);
        bool same = expected == rotated;

        REQUIRE(same == true);
    }

    SECTION("Four players. id=2")
    {
        qsizetype n = 4;
        unsigned id = 2;
        QVector<unsigned> expected = {2,3,4,1};

        QVector<unsigned> rotated = mapPlayers(n, id);
        bool same = expected == rotated;

        REQUIRE(same == true);
    }

    SECTION("Four players. id=4")
    {
        qsizetype n = 4;
        unsigned id = 4;
        QVector<unsigned> expected = {4,1,2,3};

        QVector<unsigned> rotated = mapPlayers(n, id);
        bool same = expected == rotated;

        REQUIRE(same == true);
    }

    SECTION("Id < 1")
    {
        qsizetype n = 4;
        unsigned id = 0;
        QVector<unsigned> expected = {1,2,3,4};

        QVector<unsigned> rotated = mapPlayers(n, id);
        bool same = expected == rotated;

        REQUIRE(same == true);
    }

    SECTION("Id > n")
    {
        qsizetype n = 4;
        unsigned id = 5;
        QVector<unsigned> expected = {1,2,3,4};

        QVector<unsigned> rotated = mapPlayers(n, id);
        bool same = expected == rotated;

        REQUIRE(same == true);
    }

    SECTION("No players")
    {
        qsizetype n = 0;
        unsigned id = 2;
        QVector<unsigned> expected = {};

        QVector<unsigned> rotated = mapPlayers(n, id);
        bool same = expected == rotated;

        REQUIRE(same == true);
    }

}

TEST_CASE("Cards path")
{
    SECTION("Regular card path")
    {
        QString color = "Red";
        QString shape = "CSC";
        QString specialType = "None";

        QString expectedPath = QString(":/cards/resources/img/cards/front/%1%2.png").arg(shape, color);
        QString actualPath = getPathFromCard(color, shape, specialType);

        REQUIRE(expectedPath == actualPath);
    }

    SECTION("Special card path")
    {
        QString color = "None";
        QString shape = "None";
        QString specialType = "inward";

        QString expectedPath = QString(":/cards/resources/img/cards/front/%1.png").arg(specialType);
        QString actualPath = getPathFromCard(color, shape, specialType);

        REQUIRE(expectedPath == actualPath);
    }

    SECTION("Non existing color, shape and special type attributes")
    {
        QString color = "None";
        QString shape = "None";
        QString specialType = "None";

        QString resultPath = getPathFromCard(color, shape, specialType);
        bool path = resultPath.isEmpty();

        REQUIRE(path == true);
    }

    SECTION("Regular card, no color")
    {
        QString color = "None";
        QString shape = "CG";
        QString specialType = "None";

        QString resultPath = getPathFromCard(color, shape, specialType);
        bool path = resultPath.isEmpty();

        REQUIRE(path == true);
    }

    SECTION("Regular card, no shape")
    {
        QString color = "Blue";
        QString shape = "None";
        QString specialType = "None";

        QString resultPath = getPathFromCard(color, shape, specialType);
        bool path = resultPath.isEmpty();

        REQUIRE(path == true);
    }

    SECTION("Case sensitive color")
    {
        QString color = "yellow";
        QString shape = "OSO";
        QString specialType = "None";

        QString resultPath = getPathFromCard(color, shape, specialType);
        bool path = resultPath.isEmpty();

        REQUIRE(path == true);
    }

    SECTION("Color is all upper case letters")
    {
        QString color = "BLUE";
        QString shape = "LSH";
        QString specialType = "None";

        QString resultPath = getPathFromCard(color, shape, specialType);
        bool path = resultPath.isEmpty();

        REQUIRE(path == true);
    }

    SECTION("Shape is all lower case letters")
    {
        QString color = "Blue";
        QString shape = "icb";
        QString specialType = "None";

        QString resultPath = getPathFromCard(color, shape, specialType);
        bool path = resultPath.isEmpty();

        REQUIRE(path == true);
    }

    SECTION("SpecialType is all upper case letters")
    {
        QString color = "None";
        QString shape = "None";
        QString specialType = "COLOR";

        QString resultPath = getPathFromCard(color, shape, specialType);
        bool path = resultPath.isEmpty();

        REQUIRE(path == true);
    }

}
