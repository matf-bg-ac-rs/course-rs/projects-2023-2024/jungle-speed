#include "catch2/catch_all.hpp"
#include <iostream>
#include "../src/cards/Card.h"
#include "../src/cards/Color.h"
#include "../src/cards/RegularCard.h"
#include "../src/cards/SpecialCard.h"
#include "../src/cards/SpecialType.h"
#include "../src/server/gameserver.h"
#include "../src/server/server.h"
#include "../src/player/player.h"

TEST_CASE("Player tests")
{
    SECTION("Player initialization")
    {

        Player player("anja");


        REQUIRE(player.Name() == "anja");
        REQUIRE(player.getDiscard().size()==0);
        REQUIRE(player.getDeck().size()==0);
        REQUIRE(player.getId() == 0);
    }

    SECTION("Push card to player's deck")
    {

        Player player("mina");
        Card* card = new RegularCard();


        player.pushCard(card);


        REQUIRE(player.getDeck().size() == 1);
        REQUIRE(player.getTopCard() == card);
    }

    SECTION("Open next card")
    {

        Player player("dejana");
        Card* card = new RegularCard();
        player.pushCard(card);


        player.openNextCard();





        REQUIRE(player.getDeck().size()==0);
        REQUIRE(player.getDiscard().size() == 1);
    }

    SECTION("Clear discard pile")
    {

        Player player("anja");
        Card* card1 = new RegularCard();
        Card* card2 = new SpecialCard();

        player.pushCard(card1);
        player.pushCard(card2);
        player.openNextCard();


        player.clearDiscard();


        REQUIRE(player.getDiscard().size()==0);
    }

    SECTION("Set and get player ID")
    {

        Player player("milos");


        player.setId(42);


        REQUIRE(player.getId() == 42);
    }

    SECTION("Discard to deck")
    {

        Player player("lazar");
        Card* card = new RegularCard();

        player.pushCard(card);
        player.openNextCard();


        player.discardToDeck();


        REQUIRE(player.getDeck().size() == 1);
        REQUIRE(player.getDiscard().size()==0);
        REQUIRE(player.getTopCard() == card);
    }

    SECTION("Give card to another player")
    {

        Player player1("anja");
        Player player2("mina");
        Card* card = new RegularCard();

        player1.pushCard(card);
        player1.openNextCard();


        player1.giveCard(&player2);


        REQUIRE(player1.getDeck().size()==0);
        REQUIRE(player2.getDeck().size() == 1);

    }
}
