#ifndef CLIENT_H
#define CLIENT_H

#include <QJsonArray>
#include <QJsonObject>
#include <QObject>
#include <QString>
#include <QTcpSocket>

class Client : public QObject
{
    Q_OBJECT

public:
    enum MessageType {
        Totem,
        TotemGrabbed,
        Login,
        Start,
        Winner,
        End,
        Card,
        DuelResult,
        UserDisconnected,
        Inward,
        InitialData,
        UnknownMessageType
    };
    explicit Client(QObject *parent = nullptr);
    ~Client();
    void connectToServer(const QString &host, const QString &username);
    void disconnectFromServer();
    unsigned getId();
    QString getUsername();
    static MessageType getMessageType(const QString &typeString);

signals:
    void connected();
    void disconnected();
    void errorOccurred(const QString &errorMsg);
    void totemInteractionAllowed();
    void totemInteractionNotAllowed();
    void totemGrabbed(const QString &playerMsg);
    void connectedToServer(unsigned id);
    void gameStarted(const QJsonArray &players, unsigned time);
    void gameEnded();
    void cardDrawn(unsigned playerId,
                   const QString &color,
                   const QString &shape,
                   const QString &specialType,
                   int deck_size,
                   int discard_size);
    void winnerAnnounced(const QString &winner);
    void logMessage(const QString &msg, const QString &messageType);
    void duelResult(const QString &name, const QString &result, const int  id, const int totem_cards);
    void playerDisconnect(int player_id);
    void inward(const unsigned playerId, const QString &name, const int totem_cards);

public slots:
    void sendRequest(const QString &requestType);
    void sendTotemGrabRequest();
    void sendReady();

private slots:
    void onReadyRead();
    void onDisconnected();
    void sendInitialData();

private:
    QTcpSocket *m_socket;
    QString m_username;
    unsigned m_id;
    unsigned m_port = 9000;

    void processJson(const QJsonObject &jsonObj);
};

#endif // CLIENT_H
