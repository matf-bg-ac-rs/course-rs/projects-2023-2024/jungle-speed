#include "Client.h"
#include <QString>
#include <QJsonDocument>
#include <QJsonParseError>

Client::Client(QObject *parent)
    : QObject(parent), m_socket(new QTcpSocket(this))
{
    connect(m_socket, &QTcpSocket::readyRead, this, &Client::onReadyRead);
    connect(m_socket, &QTcpSocket::disconnected, this, &Client::onDisconnected);
    connect(m_socket, &QTcpSocket::connected, this, &Client::sendInitialData);
}

Client::~Client()
{
    delete m_socket;
}

void Client::connectToServer(const QString &host, const QString &username)
{
    m_username = username;
    m_socket->connectToHost(host, m_port);
}

void Client::disconnectFromServer()
{
    m_socket->disconnectFromHost();
}

void Client::sendRequest(const QString &requestType)
{
    if (!m_socket->isOpen())
    {
        emit errorOccurred("Socket is not open");
        return;
    }

    QJsonObject jsonObj;
    jsonObj["type"] = requestType;
    if (requestType == "totemGrab")
    {
        jsonObj["player"] = m_username;
        jsonObj["id"] = QString::number(m_id);
    }

    QByteArray jsonData = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);
    m_socket->write(jsonData);
    m_socket->flush();
}

void Client::sendTotemGrabRequest()
{
    sendRequest("totemGrab");
}

void Client::sendReady()
{
    sendRequest("ready");
}

unsigned int Client::getId()
{
    return m_id;
}

QString Client::getUsername()
{
    return m_username;
}

Client::MessageType Client::getMessageType(const QString &typeString)
{
    static const QMap<QString, MessageType> typeMap = {
        {"totem", Totem},
        {"totemGrabbed", TotemGrabbed},
        {"login", Login},
        {"start", Start},
        {"winner", Winner},
        {"end", End},
        {"card", Card},
        {"duel result", DuelResult},
        {"user disconnected", UserDisconnected},
        {"inward", Inward},
    };

    return typeMap.value(typeString, UnknownMessageType);
}


void Client::onReadyRead()
{

    QByteArray jsonData = m_socket->readAll();
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(jsonData, &parseError);

    if (parseError.error != QJsonParseError::NoError)
    {
        emit errorOccurred("JSON parse error: " + parseError.errorString());
        return;
    }

    if (!doc.isObject())
    {
        emit errorOccurred("Invalid JSON document");
        return;
    }

    QJsonObject jsonObj = doc.object();
    processJson(jsonObj);
}

void Client::processJson(const QJsonObject &jsonObj)
{
    if (!jsonObj.contains("type"))
    {
        emit errorOccurred("Missing 'type' field in JSON");
        return;
    }

    MessageType messageType = getMessageType(jsonObj.value("type").toString());

    switch (messageType)
    {
    case Totem:
        if (jsonObj.value("allowed").toBool() == true)
        {
            emit totemInteractionAllowed();
        }
        else
        {
            emit totemInteractionNotAllowed();
        }
        break;

    case TotemGrabbed:
    {
        auto player = jsonObj["username"].toString();
        QString playerMsg = QString(player).append(" grabbed totem.");
        emit totemGrabbed(playerMsg);
    }
    break;

    case Login:
    {
        auto jsonVal = jsonObj["id"];
        auto id = jsonVal.toString().toInt();
        m_id = id;
        emit connectedToServer(id);
    }
    break;

    case Start:
    {
        QJsonArray players = jsonObj["players"].toArray();
        auto time = jsonObj["time"].toString().toInt();
        emit gameStarted(players, time);
    }
    break;

    case Winner:
    {
        QString winner = jsonObj["username"].toString();
        emit winnerAnnounced(winner);
    }
    break;

    case End:
        emit gameEnded();
        break;

    case Card:
    {
        auto id = jsonObj["playerId"].toString().toInt();
        QString color = jsonObj["color"].toString();
        QString shape = jsonObj["shape"].toString();
        QString specialType = jsonObj["specialType"].toString();
        auto deckSize = jsonObj["deck"].toString().toInt();
        auto discardSize = jsonObj["discard"].toString().toInt();
        emit cardDrawn(id, color, shape, specialType, deckSize, discardSize);
    }
    break;

    case DuelResult:
    {
        QString result = jsonObj["result"].toString();
        auto id = jsonObj["playerId"].toString().toInt();
        auto name = jsonObj["player"].toString();
        auto totemCards = jsonObj["totemCards"].toString().toInt();
        emit duelResult(name, result, id, totemCards);
    }
    break;

    case UserDisconnected:
    {
        auto playerId = jsonObj["id"].toString().toInt();
        if (playerId == m_id) {
            m_socket->disconnectFromHost();
        }
        emit playerDisconnect(playerId);
    }
    break;

    case Inward:
    {
        auto playerId = jsonObj["playerId"].toString().toInt();
        auto name = jsonObj["player"].toString();
        auto totemCards = jsonObj["totemCards"].toString().toInt();
        emit inward(playerId, name, totemCards);
    }
    break;

    default:
        emit errorOccurred("Unknown message type");
        break;
    }
}

void Client::sendInitialData()
{
    QJsonObject jsonObj;
    jsonObj["type"] = QStringLiteral("initialData");
    jsonObj["username"] = m_username;

    QByteArray jsonData = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);
    m_socket->write(jsonData);
    m_socket->flush();

}

void Client::onDisconnected()
{
    emit disconnected();
}
