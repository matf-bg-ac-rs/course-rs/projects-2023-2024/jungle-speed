#include "gameserver.h"
#include "server.h"
#include "../player/player.h"
#include "../cards/SpecialType.h"
#include "../cards/SpecialCard.h"
#include <QTimer>
#include <random>
#include <optional>
#include <QThread>

gameServer::gameServer(QObject* parent,const QVector<Player *> &players) : m_server(new Server(this))

{
    m_players = players;
    m_activeGame = false;
    m_deck = QVector<Card*>{};
    m_totem_cards = QVector<Card*>{};
    m_currentStage = GAME_START;
    m_compareByColor = false;
    m_inward = false;
    m_delay = 2500;
    loop = new QEventLoop();
    m_timer = new QElapsedTimer();
    m_disconnect_timer = new QElapsedTimer();

    connect(this, &gameServer::interactionComplete, loop, &QEventLoop::quit);
    connect(m_server,&Server::onPlayerReady, this, &gameServer::onPlayerReady);
    connect(m_server,&Server::onTotemGrabbed, this, &gameServer::onTotemGrabbed);
    connect(this, &gameServer::initialize, this, &gameServer::initalizeGame);
    connect(this, &gameServer::partyReady, this, &gameServer::gameInProgress);
    connect(m_server, &Server::playerAdded, this, &gameServer::playerAdded);
    connect(this, &gameServer::lobbySizeChanged, this, &gameServer::lobby);
    connect(m_server, &Server::playerDisconnected, this, &gameServer::removePlayer);
}

gameServer::~gameServer()
{
    qDeleteAll(m_totem_cards);
    qDeleteAll(m_deck);
    qDeleteAll(m_players);
    delete loop;
    delete m_timer;
    delete m_disconnect_timer;
}


// Game initalization, this function is activated when we have enough READY people in party
void gameServer::initalizeGame()
{

    initalizeDeck(m_deck);
    shuffle(m_deck);
    distribute();

    emit partyReady();

}


// Main game loop
void gameServer::gameInProgress()
{


    while(m_activeGame)
    {
        //check the stage of the game and depending on that choose next stage
        switch(m_currentStage){

        case GAME_START:    initializedGame();break;
        case OPENING_CARD: openNextCard();break;
        case OUTWARD_CARD: drawAll(); break;
        case INTERACTION_SPACE: interact();break;
        default:
            break;
        }

    }

}

//emit username of winner and end game
void gameServer::winner(){

    QString username_winner = m_winner->Name();
    m_server->getWinner(username_winner);
    m_server->endGame();
    m_activeGame = false;

}

void gameServer::losersRearrange(QVector<Player *> &players_in_duel)
{
    for(auto& player : players_in_duel)
    {
        player->discardToDeck();
    }

}

void gameServer::interact(){

    m_timer->start();

    while(!m_timer->hasExpired(m_delay) && !m_totem)
    {
        loop->processEvents();
    }
    if(m_totem)
    {
        auto res = duel();
        if (res == true)
            winner();
    }

    m_currentStage = OPENING_CARD;
}


bool gameServer::duel(){

    changeCards();
    return checkWinner();
}


void gameServer::changeCards(){

    auto duel_discard = m_duel->getDiscard();
    auto discard_size = duel_discard.size();
    auto duel_deck = m_duel->getDeck();

    if(m_inward)
    {
        m_totem = false;
        for(auto i=0;i<discard_size;++i)
        {
            m_totem_cards.emplace_back(duel_discard.front());
            duel_discard.pop_front();
        }

        m_inward = false;

        m_server->inwardReact(m_duel->Name(), m_duel->getId(), m_totem_cards.size());

        return;
    }

    if(discard_size == 0)
        return;

    auto *card = m_duel->getTopDiscardCard();

    QVector<Player*> players_in_duel = {};
    for( auto &player : m_players)
    {
        if(player->getId() == m_duel->getId())
            continue;

        if(player->getDiscard().size() == 0)
            continue;

        auto *player_card = player->getTopDiscardCard();

        if (card->compareCards(player_card, m_compareByColor)) {
            players_in_duel.emplace_back(player);
        }
    }

    unsigned n = players_in_duel.size();
    unsigned number_of_cards = 0;
    unsigned mod_cards = 0;
    bool duel_success = n != 0 ? true : false;

    if (duel_success)
    {
        number_of_cards = discard_size / n ;
        mod_cards = discard_size % n;
        for(int i = 0; i < n; ++i)
        {
            for(int j = 0; j < number_of_cards; ++j)
            {
                m_duel->giveCard(players_in_duel[i]);
            }
        }

        for(auto i=0;i<mod_cards;++i)
        {
            m_totem_cards.emplace_back(duel_discard.front());
            duel_discard.pop_front();
        }
        m_duel->clearDiscard();
        m_server->playerDuelWon(m_duel->Name(), m_duel->getId(), m_totem_cards.size());
    }

    else
    {
        for(auto& player : m_players)
        {
            if (player->getId() == m_duel->getId()) {
                continue;
            }

            n = player->getDiscard().size();
            for(auto i = 0; i < n; ++i)
            {
                player->giveCard(m_duel);
            }

            player->clearDiscard();

        }

        int totemSize = m_totem_cards.size();
        for (int i = 0; i < totemSize; ++i)
        {
            duel_deck.emplace_back(m_totem_cards.front());
            m_totem_cards.pop_front();
        }

        players_in_duel.push_back(m_duel);
        m_server->playerDuelMissed(m_duel->Name(),m_duel->getId(), m_totem_cards.size());
    }

    losersRearrange(players_in_duel);

}

void gameServer::outwardFacingCard()
{
    m_timer->start();

    while(!m_timer->hasExpired(m_delay))
    {
        loop->processEvents();
    }

    auto cards = QVector<Card*>();
    for(auto &player : m_players)
    {
        player->openNextCard();
        auto *card = player->getDiscard().last();

        if(card->getCardType() == CardType::Special){
            cards.push_back(card);

        }
        cardDrew(player);
    }

}


bool gameServer::checkWinner(){

    for(auto &player : m_players){

        if((player->getDiscard().size() + player->getDeck().size()) == 0){
            m_winner = player;
            m_currentStage = END_GAME;
            return true;
        }
    }
    return false;
}


void gameServer::handleSpecialCard(Card *card)
{
    auto *card_cast = static_cast<SpecialCard *>(card);
    auto type  = card_cast->getSpecialType();

    switch(type)
    {
    case SpecialType::colored:
        m_compareByColor = !m_compareByColor;
        break;

    case SpecialType::inwardFacing:
    {
        m_inward = true;
        break;
    }

    case SpecialType::outwardFacing:
        m_server->totemNotInteractable();
        m_currentStage = OUTWARD_CARD;
//        outwardFacingCard();
        break;

    default:
        break;
    }
}

void gameServer::setPartySize(int size)
{
    m_partySize = size;
}

void gameServer::setDeck(QVector<Card*>& deck)
{
    m_deck = QVector<Card*>(deck);
}

void gameServer::drawAll()
{
    for(auto &player : m_players)
    {
        player->openNextCard();
        cardDrew(player);
    }
    m_currentStage = OPENING_CARD;
}

void gameServer::cardDrew(Player *player)
{
    auto *card = player->getDiscard().last();
    auto discard_size = player->getDiscard().size();
    auto deck_size = player->getDeck().size();

    std::optional<SpecialType> specialType = std::nullopt;
    std::optional<Color> color = std::nullopt;
    std::optional<Shape> shape = std::nullopt;

    if(card->getCardType() == CardType::Regular)
    {
        m_inward = false;
        auto *regular_card = static_cast<RegularCard *>(card);
        color = regular_card->getColor();
        shape = regular_card->getShape();
        m_server->cardDrew(player->getId(), color, shape, specialType, deck_size, discard_size);
    }
    else
    {
        auto *special_card = static_cast<SpecialCard *>(card);
        specialType = special_card->getSpecialType();
        m_server->cardDrew(player->getId(), color, shape, specialType,deck_size,discard_size);
        handleSpecialCard(card);
    }

}

void gameServer::openNextCard(){

    m_totem = false;
    m_disconnect_timer->start();
    while(!m_disconnect_timer->hasExpired(50))
    {
        loop->processEvents();
    }
    m_current = m_players[m_playerOnMove];
    m_playerOnMove = (m_playerOnMove + 1) % m_partySize;
    m_current->openNextCard();
    cardDrew(m_current);
    if(m_currentStage == GameStages::OPENING_CARD)
    {

        m_currentStage = INTERACTION_SPACE;
        m_server->totemInteractable();
    }

}

// This function is activated when we are starting game, setting next player to move
void gameServer::initializedGame(){
    m_playerOnMove = 0;
    m_currentStage = OPENING_CARD;

}

void gameServer::initalizeDeck(QVector<Card*> &deck)
{
    QVector<Color> colors = {Color::Yellow, Color::Green, Color::Blue, Color::Red};
    QVector<Shape> shapes = {Shape::CSC, Shape::OCO, Shape::OSO, Shape::SCS,
                             Shape::VSSH, Shape::SSH, Shape::LSH, Shape::MSH,
                             Shape::SXO, Shape::XO, Shape::HXO, Shape::HHXO,
                             Shape::SB, Shape::ISB, Shape::CB, Shape::ICB,
                             Shape::CG, Shape::ICG};
    QVector<SpecialType> types = {SpecialType::inwardFacing,
                                  SpecialType::outwardFacing,
                                  SpecialType::colored};

    for(auto color : colors)
    {
        for(auto shape : shapes)
        {
            RegularCard  *card = new RegularCard(color,shape);
            deck.emplace_back(card);
        }
    }

    for(auto type : types){

        if(type == SpecialType::inwardFacing){
            SpecialCard *card = new SpecialCard(type);
            deck.emplace_back(card);
            deck.emplace_back(card);

        }else if( type == SpecialType::outwardFacing){
            SpecialCard *card = new SpecialCard(type);
            deck.emplace_back(card);
        }else{
            SpecialCard *card = new SpecialCard(type);
            deck.emplace_back(card);
            deck.emplace_back(card);
            deck.emplace_back(card);

        }
    }

}

void gameServer::shuffle(QVector<Card*>& deck)
{
    auto rng = std::default_random_engine {13};
    std::shuffle(std::begin(deck), std::end(deck), rng);
}

void gameServer::distribute()
{
    unsigned next_player = 0;
    unsigned n = m_deck.size();
    unsigned deck_div = n / m_partySize;
    unsigned deck_mod = n % m_partySize;

    for(int i = 0; i < n - deck_mod; ++i)
    {
        m_players[next_player]->pushCard(m_deck[i]);
        next_player = (next_player + 1) % m_partySize;
    }

    std::copy(m_deck.begin() + (deck_div * m_partySize),m_deck.end(),std::back_inserter(m_totem_cards));

}


void gameServer::onTotemGrabbed(const QJsonObject &doc)
{
    m_totem = true;
    auto id = doc["id"].toString().toInt();

    for(auto &player : m_players){
        if(player->getId() == id){
            m_duel = player;
            break;
        }
    }

    emit interactionComplete();

}

void gameServer::startedDuel(QJsonObject &msg){

    auto username = msg["username"].toString();
    for(auto player : m_players){
        if(username.compare(player->Name())){
            m_duel = player;
            break;
        }
    }
}

void gameServer::playerAdded(QString username, int id)
{

    Player *player = new Player(username);
    player->setId(id);
    m_players.push_back(player);
    emit lobbySizeChanged(m_players.size());
}

void gameServer::onPlayerReady()
{
    m_readySize++;
    if(m_partySize == m_readySize)
    {
        emit initialize();
    }
}

void gameServer::lobby(int x)
{
    if (x == 4)
    {
        m_partySize = x;
        m_activeGame = true;
        m_server->startGame(m_players, m_delay);
    }
}

void gameServer::removePlayer(int id)
{

    if(m_partySize > 0)
    {
        bool found = false;
        auto i = 0;
        for(auto& x : m_players)
        {
            if (x->getId() == id){
                --m_partySize;
                found = true;
                break;
            }
            ++i;
        }
        if(found)
            m_players.removeAt(i);
    }

    if(m_partySize == 0)
    {
        std::exit(0);
    }

    emit interactionComplete();

}

void gameServer::toggleStartServer() const
{
    if(!m_server->listen(QHostAddress::LocalHost, m_port))
    {
        qDebug() << "Could not start server !";
    }
    else
    {
        qDebug() << "Listening on port" << m_port;
    }
}


