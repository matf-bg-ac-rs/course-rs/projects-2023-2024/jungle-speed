#ifndef GAMESERVER_H
#define GAMESERVER_H

#include <QObject>
#include <QTcpServer>
#include <QVector>
#include "../cards/Card.h"
#include <QTime>
#include <QElapsedTimer>
#include <QEventLoop>

class QTimer;
class Server;
class Player;
class gameServer : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(gameServer)
public:
    explicit gameServer(QObject* parent = nullptr,const QVector<Player*>&players = {});
    ~gameServer();

    // Game Stages
    enum GameStages
    {
        GAME_START,   // Players are ready and we are starting game
        OPENING_CARD, // opening card.
        INTERACTION_SPACE, //  time space for interacting with totem
        DUEL,        // State in which players card trading
        OUTWARD_CARD,
        END_GAME    // When first player get rid off all of his cards
    };




    void toggleStartServer() const;

private slots:
    void onPlayerReady();
    void onTotemGrabbed(const QJsonObject &doc);
    void initalizeGame(); // connected with Server::startGame
    void gameInProgress();
    void startedDuel(QJsonObject &msg);
    void playerAdded(QString username, int id);
    void lobby(int x);
    void removePlayer(int id);

signals:
    void partyReady(); // on this signal game should start
    void totemInteractionAllowed();
    void totemInteractionDisable();
    void getWinner(QJsonObject &winner);
    void gameEnded();
    void lobbySizeChanged(int x);
    void startGame();
    void initialize();
    void interactionComplete();

public:
    bool m_activeGame;
    Server *m_server;
    QElapsedTimer* m_timer;
    QElapsedTimer* m_disconnect_timer;
    QEventLoop* loop;
    unsigned m_delay;

private:
    static void initalizeDeck(QVector<Card *> &deck);
    static void shuffle(QVector<Card*>& deck);
    void distribute();
    bool duel();
    void cardDrew(Player *player);
    void openNextCard();
    void initializedGame();
    void interact();
    bool checkWinner();
    void changeCards();
    void winner();
    void losersRearrange(QVector<Player*> &players_in_duel);
    void outwardFacingCard();
    void handleSpecialCard(Card *card);
    void setPartySize(int size);
    void setDeck(QVector<Card*> &deck);
    void drawAll();


private:
    QVector<Player*> m_players;
    QVector<Card*> m_deck;
    GameStages m_currentStage;
    bool m_totem = false;
    Player *m_winner = nullptr;
    Player *m_duel = nullptr;
    Player *m_current = nullptr;
    unsigned m_partySize = 0;
    unsigned m_readySize = 0;
    unsigned m_playerOnMove = 0;
    unsigned m_port = 9000;
    bool m_compareByColor;
    bool m_inward;
    QVector<Card*> m_totem_cards;
};

#endif // GAMESERVER_H
