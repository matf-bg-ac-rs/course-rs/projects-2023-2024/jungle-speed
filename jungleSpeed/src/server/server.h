#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#include <QString>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include "worker.h"
#include "gameserver.h"
#include "../cards/Color.h"
#include "../cards/Shape.h"

class Player;
class Server : public QTcpServer
{
    Q_OBJECT
    Q_DISABLE_COPY(Server)
public:
    explicit Server(QObject *parent = nullptr);
    ~Server();
    void startGame(QVector<Player*>& players, unsigned time);
    void getWinner(QString winner);
    void cardDrew(unsigned id, std::optional<Color> color, std::optional<Shape> shape, std::optional<SpecialType> specialType, int deck_size, int discard_size);
    void totemInteractable();
    void totemNotInteractable();
    void endGame();
    void playerDuelWon(QString name, int id, int totem_cards);
    void playerDuelMissed(QString name, int id, int totem_cards);
    void inwardReact(QString name, int id, int totem_cards);

signals:
    void onPlayerReady();
    void onTotemGrabbed(const QJsonObject &doc);
    void playerAdded(QString username, int id);
    void playerDisconnected(int id);


private slots:
    void handleDisconnect(worker* w);
    void broadcast(const QJsonObject &message, worker *exclude);
    void jsonReceived(worker *sender, const QJsonObject &doc);


private:
    void sendJson(worker *des, const QJsonObject &message);
    void jsonFromUnknown(worker *sender, const QJsonObject &doc);
    void handleJson(worker *sender, const QJsonObject &doc);

    QVector<worker*> m_connections;
    const unsigned m_port = 9000;
    gameServer *m_game;

public:
    inline static unsigned m_nextId = 0;
    // QTcpServer interface
protected:
    void incomingConnection(qintptr handle);
};

#endif // SERVER_H
