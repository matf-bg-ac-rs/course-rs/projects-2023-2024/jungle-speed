#include "worker.h"

worker::worker(QObject *parent, unsigned id)
    : QObject{parent}, m_serverSocket(new QTcpSocket(this)), m_id(id)
{

    connect(m_serverSocket, &QTcpSocket::channelReadyRead, this, &worker::receiveJson);
    connect(m_serverSocket, &QTcpSocket::disconnected, this, &worker::disconnectFromClient);

}

worker::~worker()
{
    delete m_serverSocket;
}

bool worker::setSocketDescriptor(qintptr socketDescriptor)
{
    return m_serverSocket->setSocketDescriptor(socketDescriptor);
}

QString worker::getUserName() const
{
    return m_userName;
}

void worker::setUserName(const QString &userName)
{
    m_userName = userName;
}

void worker::sendJson(const QJsonObject &json)
{

    const QByteArray jsonData = QJsonDocument(json).toJson(QJsonDocument::Compact);


    m_serverSocket->write(jsonData);
    m_serverSocket->flush();

}

unsigned int worker::getId() const
{
    return m_id;
}

void worker::disconnectFromClient()
{
    m_serverSocket->disconnectFromHost();
    emit disconnectedFromClient(this);
}

void worker::receiveJson(int channelIndex)
{

    m_serverSocket->setCurrentReadChannel(channelIndex);
    QByteArray jsonData = m_serverSocket->readAll();

    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(jsonData, &parseError);

    if(parseError.error != QJsonParseError::NoError)
    {

        return;
    }

    if (!doc.isObject())
    {

        return;
    }

    QJsonObject jsonObj = doc.object();
    emit jsonReceived(this,jsonObj);

}


