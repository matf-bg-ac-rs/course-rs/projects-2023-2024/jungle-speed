#include <QStringLiteral>
#include <QJsonDocument>
#include <QObject>
#include "server.h"
#include "../player/player.h"
#include <QThread>

Server::Server(QObject *parent)
    : QTcpServer{parent}
{
}

Server::~Server()
{
    qDeleteAll(m_connections);
}



void Server::incomingConnection(qintptr handle)
{
    qDebug() <<handle << " Connecting...";

    worker* w = new worker(this, ++m_nextId);


    if(!w->setSocketDescriptor(handle))
    {
        w->deleteLater();
        return;
    }

    connect(w, &worker::disconnectedFromClient, this,&Server::handleDisconnect);
    connect(w, &worker::jsonReceived, this,&Server::jsonReceived);

    m_connections.push_back(w);
}


void Server::handleDisconnect(worker *w)
{

    QJsonObject discMessage;
    discMessage[QStringLiteral("type")] = QStringLiteral("user disconnected");
    discMessage[QStringLiteral("username")] = w->getUserName();
    discMessage[QStringLiteral("id")] = QString::number(w->getId());
    broadcast(discMessage, w);
    m_connections.removeAll(w);
    w->deleteLater();
    emit playerDisconnected(w->getId());

}


void Server::sendJson(worker *des, const QJsonObject &message)
{
    Q_ASSERT(des);
    des->sendJson(message);
}

void Server::broadcast(const QJsonObject &message, worker *exclude)
{
    QThread::msleep(45);
    for(auto& w: m_connections)
    {
        if(w == exclude)
            continue;
        sendJson(w, message);
    }
}

void Server::jsonReceived(worker *sender, const QJsonObject &doc)
{

    if (sender->getUserName().isEmpty())
        return jsonFromUnknown(sender, doc);

    return handleJson(sender, doc);

}


void Server::jsonFromUnknown(worker *sender, const QJsonObject &doc)
{
    Q_ASSERT(sender);
    auto messageType = doc.value("type").toString();

    if(messageType == "initialData")
    {
        auto username = doc["username"].toString();
        sender->setUserName(username);

        QJsonObject msg;
        msg[QStringLiteral("type")] = QStringLiteral("login");
        msg[QStringLiteral("id")] = QString::number(sender->getId());
        msg[QStringLiteral("approved")] = QStringLiteral("Connection passed");
        sendJson(sender, msg);
        emit playerAdded(username,sender->getId());

    }

}

void Server::handleJson(worker *sender, const QJsonObject &doc)
{

    Q_ASSERT(sender);

    auto messageType = doc.value("type").toString();

    if(messageType == "ready")
    {
        //qDebug() << "handleJson ready";
        QJsonObject message;
        message["type"] = QString(messageType);
        message["playerId"] = QString::number(sender->getId());
        broadcast(message, nullptr);
        emit onPlayerReady();
    }
    else if(messageType == "totemGrab")
    {
        QJsonObject message;
        message["type"] = QString(messageType);
        message["playerId"] = QString::number(sender->getId());
        message["username"] = QString(sender->getUserName());
        broadcast(message, nullptr);
        emit onTotemGrabbed(doc); // gameServer handles from there.
    }

}

void Server::startGame(QVector<Player *> &players, unsigned time)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("start");
    QJsonArray playersArray;

    for(auto& x : players)
    {
        QJsonObject player;
        player[QStringLiteral("id")] = QString::number(x->getId());
        player[QStringLiteral("username")] = x->Name();

        playersArray.append(player);

    }

    message[QStringLiteral("players")] = playersArray;
    message[QStringLiteral("time")] = QString::number(time);
    broadcast(message, nullptr);
}

void Server::getWinner(QString winner)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("winner");
    message[QStringLiteral("username")] = winner;
    broadcast(message, nullptr);
}

void Server::cardDrew(unsigned id, std::optional<Color> color, std::optional<Shape> shape, std::optional<SpecialType> specialType,int deck_size, int discard_size)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("card");
    message[QStringLiteral("playerId")] = QString::number(id);
    message[QStringLiteral("deck")] = QString::number(deck_size);
    message[QStringLiteral("discard")] = QString::number(discard_size);

    if(specialType.has_value())
    {
        message[QStringLiteral("specialType")] = ToString(specialType.value());
    }
    else
    {
        message[QStringLiteral("specialType")] = QStringLiteral("None");
    }

    if(color.has_value())
    {
        message[QStringLiteral("color")] = ToString(color.value());
    }
    else
    {
         message[QStringLiteral("color")] = QStringLiteral("None");
    }

    if(shape.has_value())
    {
        message[QStringLiteral("shape")] = ToString(shape.value());
    }
    else
    {
        message[QStringLiteral("shape")] = QStringLiteral("None");
    }


    broadcast(message,nullptr);
}

void Server::totemInteractable()
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("totem");
    message["allowed"] = true;
    broadcast(message, nullptr);
}

void Server::totemNotInteractable()
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("totem");
    message["allowed"] = false;
    broadcast(message, nullptr);
}

void Server::endGame()
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("end");
    broadcast(message, nullptr);
}

void Server::playerDuelWon(QString name, int id, int totem_cards)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("duel result");
    message[QStringLiteral("result")] = QStringLiteral("won");
    message[QStringLiteral("player")] = name;
    message[QStringLiteral("playerId")] = QString::number(id);
    message[QStringLiteral("totemCards")] = QString::number(totem_cards);
    broadcast(message, nullptr);


}

void Server::playerDuelMissed(QString name, int id, int totemCards)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("duel result");
    message[QStringLiteral("result")] = QStringLiteral("lost");
    message[QStringLiteral("player")] = name;
    message[QStringLiteral("playerId")] = QString::number(id);
    message[QStringLiteral("totemCards")] = QString::number(totemCards);
    broadcast(message,nullptr);
}

void Server::inwardReact(QString name, int id, int totem_cards)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("inward");
    message[QStringLiteral("player")] = name;
    message[QStringLiteral("playerId")] = QString::number(id);
    message[QStringLiteral("totemCards")] = QString::number(totem_cards);
    broadcast(message, nullptr);
}
