#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QTcpSocket>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonParseError>

class worker : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(worker)
public:
    explicit worker(QObject *parent = nullptr, unsigned id = 0);
    ~worker();
    virtual bool setSocketDescriptor(qintptr socketDescriptor);
    QString getUserName() const;
    void setUserName(const QString &userName);
    void sendJson(const QJsonObject &json);
    unsigned getId() const;


signals:
    void jsonReceived(worker *sender, const QJsonObject &jsonDoc);
    void disconnectedFromClient(worker *sender);
    void logMessage(const QString &msg);


public slots:
    void disconnectFromClient();

private slots:
    void receiveJson(int channelIndex);

private:
    QTcpSocket *m_serverSocket;
    QString m_userName;
    unsigned m_id;
};

#endif // WORKER_H
