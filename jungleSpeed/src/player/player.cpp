#include "player.h"

Player::Player(QString userName) : m_userName(userName),
    m_discard{},
    m_deck{}
{
}

QString Player::Name()
{
    return m_userName;
}

QVector<Card*> Player::getDiscard()
{
    return m_discard;
}

QVector<Card*> Player::getDeck()
{
    return m_deck;
}

void Player::pushCard(Card* card)
{
    m_deck.emplace_back(card);
}

void Player::openNextCard(){

    if(m_deck.size() == 0)
        discardToDeck();

    auto current_card = m_deck.first();
    m_deck.pop_front();
    m_discard.emplace_back(current_card);

}

Card* Player::getTopDiscardCard(){

    return m_discard.back();
}

Card* Player::getTopCard(){
    return m_deck.back();
}

void Player::clearDiscard()
{
    m_discard.clear();
}

void Player::setId(const unsigned id)
{
    m_playerId = id;
}

unsigned int Player::getId() const
{
    return m_playerId;
}

void Player::discardToDeck()
{
    auto size = m_discard.size();

    for(auto i = 0; i < size; ++i)
    {
        m_deck.emplace_back(m_discard[i]);
    }
    m_discard.clear();

}

void Player::giveCard(Player *enemy){


    enemy->pushCard(m_discard.front());
    m_discard.pop_front();

}


