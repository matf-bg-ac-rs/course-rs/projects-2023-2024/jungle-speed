#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QString>
#include <QDebug>
#include "../cards/Card.h"
#include "../cards/RegularCard.h"

class Player
{
public:
    Player(QString userName = QString(""));
    QString Name();
    QVector<Card*> getDiscard();
    QVector<Card*> getDeck();
    void pushCard(Card* card);
    void openNextCard();
    void giveCard(Player *enemy);
    Card* getTopCard();
    void clearDiscard();
    void setId(const unsigned id);
    unsigned getId() const;
    void discardToDeck();
    Card* getTopDiscardCard();


private:
    QString m_userName;
    QVector<Card*> m_discard;
    QVector<Card*> m_deck;
    unsigned m_playerId;
};

#endif // PLAYER_H
