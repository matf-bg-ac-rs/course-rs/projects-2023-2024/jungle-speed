#ifndef SHAPE_H
#define SHAPE_H

#include "SpecialType.h"
enum class Shape
{
    CSC = 1,
    OCO = 2,
    OSO = 3,
    SCS = 4,
    VSSH = 5,
    SSH = 6,
    LSH = 7,
    MSH = 8,
    SXO = 9,
    XO = 10,
    HXO = 11,
    HHXO = 12,
    SB = 13,
    ISB = 14,
    CB = 15,
    ICB = 16,
    CG = 17,
    ICG = 18
};


inline const char* ToString(Shape v)
{
    switch (v)
    {
    case Shape::CSC:   return "CSC";
    case Shape::OCO:   return "OCO";
    case Shape::OSO:   return "OSO";
    case Shape::SCS:   return "SCS";
    case Shape::VSSH:   return "VSSH";
    case Shape::SSH:   return "SSH";
    case Shape::LSH:   return "LSH";
    case Shape::MSH:   return "MSH";
    case Shape::SXO:   return "SXO";
    case Shape::XO:   return "XO";
    case Shape::HXO:   return "HXO";
    case Shape::HHXO:   return "HHXO";
    case Shape::SB:   return "SB";
    case Shape::ISB:   return "ISB";
    case Shape::CB:   return "CB";
    case Shape::ICB:   return "ICB";
    case Shape::CG:   return "CG";
    case Shape::ICG:   return "ICG";
    default:
        return "";
        break;
    }
}
#endif // SHAPE_H
