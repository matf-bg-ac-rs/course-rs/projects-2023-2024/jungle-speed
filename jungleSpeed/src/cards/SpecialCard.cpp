#include "SpecialCard.h"

SpecialCard::SpecialCard( SpecialType type)
    : Card(), type(type)
{
}

SpecialType SpecialCard::getSpecialType() const
{
    return type;
}

QString SpecialCard::displayCard() const
{
    return QString(ToString(this->getCardType()));
}
