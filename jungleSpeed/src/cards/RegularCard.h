#ifndef REGULARCARD_H
#define REGULARCARD_H

#include "Card.h"
#include "Color.h"
#include "Shape.h"

class RegularCard : public Card
{
public:
    RegularCard(Color color=Color::Blue, Shape shape=Shape::CG);
    ~RegularCard() = default;

private:
    Color color;
    Shape shape;

    // Card interface
public:
    Color getColor() const;
    Shape getShape() const;
    CardType getCardType() const override { return CardType::Regular; }


    // Card interface
public:
    QString displayCard() const override;
};

#endif // REGULARCARD_H
