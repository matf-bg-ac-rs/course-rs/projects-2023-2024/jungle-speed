#ifndef SPECIALTYPE_H
#define SPECIALTYPE_H

#include<QString>

enum class SpecialType
{
    inwardFacing,
    outwardFacing,
    colored
};

inline const QString ToString(SpecialType v)
{
    switch (v)
    {
    case SpecialType::inwardFacing:   return QString("inward");
    case SpecialType::outwardFacing:   return QString("outward");
    case SpecialType::colored: return QString("color");
    default:
        return QString("");
        break;
    }
}
#endif // SPECIALTYPE_H
