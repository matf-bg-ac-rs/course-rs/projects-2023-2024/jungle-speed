#include "RegularCard.h"

RegularCard::RegularCard(Color color, Shape shape)
    : Card(), color(color), shape(shape)
{
}

Color RegularCard::getColor() const
{
    return color;
}

Shape RegularCard::getShape() const
{
    return shape;
}

QString RegularCard::displayCard() const
{
    return QString("Color:") + ToString(color) + QString( "Shape: ") + ToString(shape);
}


