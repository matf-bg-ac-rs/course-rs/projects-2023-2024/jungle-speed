#ifndef SPECIALCARD_H
#define SPECIALCARD_H

#include "Card.h"
#include "SpecialType.h"

class SpecialCard : public Card
{
public:
    SpecialCard(SpecialType type = SpecialType::colored);
    ~SpecialCard() = default;

private:
    SpecialType type;

public:
    SpecialType getSpecialType() const;
    CardType getCardType() const override { return CardType::Special; }

    // Card interface
public:
    QString displayCard() const override;
};

#endif // SPECIALCARD_H
