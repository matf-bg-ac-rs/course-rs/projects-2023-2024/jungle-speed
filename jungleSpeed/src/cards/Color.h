#ifndef COLOR_H
#define COLOR_H

#include <QString>
enum class Color
{
    Red = 'r',
    Green = 'g',
    Blue = 'b',
    Yellow = 'y'
};

inline const QString ToString(Color v)
{
    switch (v)
    {
    case Color::Red:   return QString("Red");
    case Color::Green:   return QString("Green");
    case Color::Blue:   return QString("Blue");
    case Color::Yellow:   return QString("Yellow");

    default:
        return QString("");
        break;
    }

}

#endif // COLOR_H
