#ifndef CARD_H
#define CARD_H

#include <QString>

enum class CardType { Regular, Special };

inline const QString ToString(CardType type) {
    switch (type) {
    case CardType::Regular: return QString("Regular");
    case CardType::Special: return QString("Special");
    default: return QString("Unknown");
    }
}

class Card
{
public:
    Card();
    virtual ~Card();
    virtual QString displayCard() const = 0;
    virtual CardType getCardType() const = 0;

    bool compareCards(Card *other, bool byColor);

};

#endif // CARD_H
