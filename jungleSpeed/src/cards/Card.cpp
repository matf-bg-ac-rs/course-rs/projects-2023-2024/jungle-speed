#include "Card.h"
#include "RegularCard.h"

Card::Card()
{
}

Card::~Card()
{
}

bool Card::compareCards(Card *other, bool byColor)
{

    if((other->getCardType() ==  CardType::Regular) && (this->getCardType() ==  CardType::Regular))
    {
        auto first = static_cast<RegularCard*>(other);
        auto second = static_cast<RegularCard*>(this);


        if(byColor){

            if(first->getColor() == second->getColor())
                return true;
            return false;
        }else{
            if(first->getShape() == second->getShape())
                return true;
            return false;

        }

    }
    return false;

}

