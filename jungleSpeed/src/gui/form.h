#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include "../client/Client.h"
#include "game.h"
#include "music.h"

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = nullptr);
    ~Form();

private:
    Ui::Form *ui;
    Client *client;
    Game *gameWindow;
    Music *m_music_player;



private slots:
    void pbNewGame_clicked();
    void pbInstructions_clicked();
    void pbSettings_clicked();
    void pbMainMenu2_clicked();
    void pbMainMenu3_clicked();
    void pbMainMenu1_clicked();
    void pbMainMenu4_clicked();
    void pbSpecialCards_clicked();
    void pbHowToPlay_clicked();

    void pbInstructions1_clicked();
    void pbInstructions2_clicked();
    void pbJoinGame_clicked();

    void handleConnectedToServer();
    void handleGameStarted(const QJsonArray &players, unsigned time);
    void handleGameEnded();
    void handleWinner(const QString &winner);

    void pbSound_clicked();
    void hsSound_valueChanged(int value);

    void pbSoundOff_clicked();
    void handleMusic();
    void exit(int player_id);

};

#endif // FORM_H
