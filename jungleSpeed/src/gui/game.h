#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QLabel>
#include "faceuppileview.h"
#include "facedownpileview.h"
#include "../client/Client.h"

namespace Ui {
class Game;
}

class Game : public QWidget
{
    Q_OBJECT

public:
    explicit Game(qsizetype numPlayers, unsigned id, unsigned time, QWidget *parent = nullptr);
    ~Game();
    void setUsernames(QList<QString> &usernames);
    unsigned getPosFromId(unsigned id);
    unsigned getIdFromPos(unsigned pos);
    void setFaceDownCards();
    void setFaceUpCards();
    void turnCardOver(const unsigned playerId, const QString& color, const QString& shape, const QString& specialType, const int deck_size, const int discard_size);
    void write_in_chatBox(const QString msg, const QString messageType);
    void handleTotemGrabbed(const QString playerMsg);
    void handleTotemInteractionAllowed();
    void handleInward(const unsigned id, const QString &name, const int totem_cards);
    void updateTotem();
    void printDuelResult(const QString &name, const QString &result, const int  id, const int totem_cards);

private slots:
    void pbTotem_clicked();
signals:
    void sendTotemGrabRequest();
private:
    Ui::Game *ui;
    bool ind;
    qsizetype m_numPlayers;
    Client* m_client;
    unsigned m_id;
    QList<QLabel*> uiPlayers;
    QList<QLabel*> uiLabels;
    QList<QLabel*> uiDiscardLabels;
    QMap<unsigned, unsigned> id_to_pos;
    QMap<unsigned, unsigned> pos_to_id;
    QList<FaceDownPileView*> uiFaceDownCards;
    QList<FaceUpPileView*> uiFaceUpCards;
    unsigned player_id;
    unsigned m_time;
    QTimer *timer;
};

#endif // GAME_H
