#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QVector>
#include <QString>

QVector<unsigned> mapPlayers(qsizetype n, unsigned id);
QString getPathFromCard(const QString &color, const QString &shape, QString specialType);

#endif // FUNCTIONS_H
