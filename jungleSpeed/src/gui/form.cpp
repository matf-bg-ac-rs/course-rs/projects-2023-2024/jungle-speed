#include "form.h"
#include "../../forms/ui_form.h"
#include <QNetworkInterface>
#include <QStackedWidget>
#include <QMovie>
#include <QLabel>
#include <QDebug>
#include <QSize>

Form::Form(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Form),
    client(new Client(this)),
    gameWindow(nullptr)
{
    ui->setupUi(this);
    this->setWindowTitle("Jungle Speed");
    this->setWindowIcon(QIcon(":/cards/resources/img/cards/back/card_back.png"));
    ui->stackedWidget->setCurrentIndex(0);
    m_music_player= new Music(true,ui->hsSound->value());


    connect(ui->hsSound, &QSlider::valueChanged, this, &Form::hsSound_valueChanged);
    connect(ui->pbNewGame, &QPushButton::clicked, this, &Form::pbNewGame_clicked);
    connect(ui->pbInstructions, &QPushButton::clicked, this, &Form::pbInstructions_clicked);
    connect(ui->pbSettings, &QPushButton::clicked, this, &Form::pbSettings_clicked);
    connect(ui->pbMainMenu2, &QPushButton::clicked, this, &Form::pbMainMenu2_clicked);
    connect(ui->pbMainMenu3, &QPushButton::clicked, this, &Form::pbMainMenu3_clicked);
    connect(ui->pbMainMenu1, &QPushButton::clicked, this, &Form::pbMainMenu1_clicked);
    connect(ui->pbMainMenu4, &QPushButton::clicked, this, &Form::pbMainMenu4_clicked);
    connect(ui->pbSpecialCards, &QPushButton::clicked, this, &Form::pbSpecialCards_clicked);
    connect(ui->pbHowToPlay, &QPushButton::clicked, this, &Form::pbHowToPlay_clicked);
    connect(ui->pbInstructions1, &QPushButton::clicked, this, &Form::pbInstructions1_clicked);
    connect(ui->pbInstructions2, &QPushButton::clicked, this, &Form::pbInstructions2_clicked);
    connect(ui->pbJoinGame, &QPushButton::clicked, this, &Form::pbJoinGame_clicked);
    connect(ui->pbSound, &QPushButton::clicked, this, &Form::pbSound_clicked);
    connect(ui->pbSoundOff, &QPushButton::clicked, this, &Form::pbSoundOff_clicked);


}

Form::~Form()
{
    delete ui;
}
void Form::pbNewGame_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);

}
void Form::pbInstructions_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);

}
void Form::pbSettings_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);

}
void Form::pbMainMenu1_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);

}
void Form::pbMainMenu2_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);

}
void Form::pbMainMenu3_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);

}
void Form::pbHowToPlay_clicked()
{
    ui->stackedWidget->setCurrentIndex(4);

}
void Form::pbSpecialCards_clicked()
{
    ui->stackedWidget->setCurrentIndex(5);

}



void Form::pbInstructions1_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}


void Form::pbInstructions2_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void Form::pbMainMenu4_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void Form::pbJoinGame_clicked()
{
    QString username = ui->leUsername->text();

    qDebug() << "btStart clicked!";
    qDebug() << username;


    ui->pbJoinGame->setText("Connecting...");


    client->connectToServer("localhost", username);

    connect(client,&Client::gameStarted, this , &Form::handleGameStarted);
    connect(client, &Client::connectedToServer, this, &Form::handleConnectedToServer);


}

void Form::handleConnectedToServer()
{
    ui->pbJoinGame->setText("Connected");
    ui->stackedWidget->setCurrentIndex(6);
    QMovie *movie = new QMovie(":/gifs/resources/waiting.gif");

    qDebug() << movie->isValid();

    ui->lblGif->setMovie(movie);
    movie->start();

}

void Form::handleGameStarted(const QJsonArray &players, unsigned time)
{
    gameWindow = new Game(players.size(), client->getId(), time);

//    gameWindow->show();
//    this->hide();

    qDebug() << "Game started";


    QList<QString> usernames = {};

    unsigned numPlayers = players.size();

    for(int i = 0; i < numPlayers; i++)
    {
        usernames.push_back(QString(players[i]["username"].toString()));
    }

    gameWindow->setUsernames(usernames);

    ui->stackedWidget->addWidget(gameWindow);
    ui->stackedWidget->setCurrentIndex(8);
    ui->stackedWidget->setStyleSheet("background: rgb(34, 38, 23)");

    connect(client, &Client::cardDrawn, gameWindow, &Game::turnCardOver);
    connect(client, &Client::logMessage, gameWindow, &Game::write_in_chatBox);
    connect(client, &Client::totemGrabbed, gameWindow, &Game::handleTotemGrabbed);
    connect(client, &Client::winnerAnnounced, this, &Form::handleWinner);
    connect(client, &Client::gameEnded, this, &Form::handleGameEnded);
    connect(client, &Client::totemInteractionAllowed, gameWindow, &Game::handleTotemInteractionAllowed);
    connect(client, &Client::totemInteractionNotAllowed, gameWindow, &Game::updateTotem);
    connect(client, &Client::duelResult, gameWindow, &Game::printDuelResult);
    connect(client, &Client::playerDisconnect, this, &Form::exit);
    connect(gameWindow, &Game::sendTotemGrabRequest, client, &Client::sendTotemGrabRequest);
    connect(client, &Client::inward, gameWindow, &Game::handleInward);

    client->sendReady();
}
void Form::handleGameEnded()
{
    ui->stackedWidget->setCurrentIndex(7);
    ui->stackedWidget->setStyleSheet("background-image: url(:/backgrounds/resources/img/backgrounds/background.png)");
    //ui->lbWinner->setText(winner);

    //ui->stackedWidget->setCurrentIndex(0);
}

void Form::handleWinner(const QString &winner)
{
    ui->stackedWidget->setCurrentIndex(7);
    ui->lbWinner->setText(winner);

}

void Form::pbSound_clicked()
{
    handleMusic();
    return;
}

void Form::pbSoundOff_clicked()
{
    handleMusic();
    return;
}

void Form::hsSound_valueChanged(int value)
{
    m_music_player->setMusicVolume(value);
    m_music_player->setOldVolume(value);
}

void Form::handleMusic()
{
    if(m_music_player->musicStatus())
    {
        m_music_player->changeState();
        ui->hsSound->setDisabled(true);
        ui->hsSound->setStyleSheet("background: rgba(50, 50, 0, 150); border: none; color: rgb(255, 225, 117); selection-background-color: rgb(119, 118, 123);");
        ui->pbSound->setStyleSheet("image:url(':/sound buttons/resources/img/sound buttons/sound_on.png')");
        ui->pbSoundOff->setStyleSheet("image:url(':/sound buttons/resources/img/sound buttons/sound_on.png')");
    }
    else
    {
        m_music_player->changeState();
        ui->hsSound->setDisabled(false);
        ui->hsSound->setStyleSheet("background: rgba(50, 50, 0, 150); border: none; color: rgb(255, 225, 117);");
        ui->pbSound->setStyleSheet("image:url(':/sound buttons/resources/img/sound buttons/sound_off.png')");
        ui->pbSoundOff->setStyleSheet("image:url(':/sound buttons/resources/img/sound buttons/sound_off.png')");
    }
}

void Form::exit(int player_id)
{
    if(player_id == client->getId())
        this->close();
}




