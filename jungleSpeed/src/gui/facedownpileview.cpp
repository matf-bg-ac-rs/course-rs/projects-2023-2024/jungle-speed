#include "facedownpileview.h"

FaceDownPileView::FaceDownPileView(QWidget *parent, int x, int y, int width, int height)
{
    m_label = new QLabel(parent);
    m_label->setGeometry(x, y, width, height);
    this->setFull();
}

void FaceDownPileView::setEmpty()
{
    m_label->setStyleSheet("");
}

void FaceDownPileView::setFull()
{
    m_label->setStyleSheet("image: url(':/cards/resources/img/cards/back/card_back.png')");
}
