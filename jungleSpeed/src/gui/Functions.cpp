#include "Functions.h"


QVector<unsigned int> mapPlayers(qsizetype n, unsigned int id)
{
    QVector<unsigned> rotated = {};
    for (unsigned i = 1; i <= n; i++)
    {
        rotated.push_back(i);
    }

    if (id < 1 || id > n)
        return rotated;

    auto iter = rotated.begin() + id - 1;
    std::rotate(rotated.begin(), iter, rotated.end());

    return rotated;
}

QString getPathFromCard(const QString &color, const QString &shape, QString specialType)
{
    QString path;

    if(specialType == "None" && (color == "None" || shape == "None"))
    {
        return QString("");
    }

    if (color.at(0).isLower())
    {
        return QString("");
    }

    if(color.isUpper() || shape.isLower() || specialType.isUpper())
    {
        return QString("");
    }

    if (specialType == "None")
    {
        path = QString(":/cards/resources/img/cards/front/%1%2.png").arg(shape, color);
    }
    else
    {
        path = QString(":/cards/resources/img/cards/front/%1.png").arg(specialType);
    }

    return path;
}
