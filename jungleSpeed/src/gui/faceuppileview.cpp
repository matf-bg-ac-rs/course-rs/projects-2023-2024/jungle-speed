#include "faceuppileview.h"

#include <QLabel>
#include <QString>

FaceUpPileView::FaceUpPileView(QWidget *parent, int x, int y, int width, int height)
{
    m_label = new QLabel(parent);
    m_label->setGeometry(x, y, width, height);
    m_label->setStyleSheet(m_style);
}

void FaceUpPileView::setImage(QString imagePath)
{
    m_style = QString("image: url('%1')").arg(imagePath);
    m_label->setStyleSheet(m_style);
}

void FaceUpPileView::setEmpty()
{
    m_label->setStyleSheet("");
}
