#ifndef FACEUPPILEVIEW_H
#define FACEUPPILEVIEW_H

#include <QString>
#include <QLabel>

class FaceUpPileView
{
public:
    FaceUpPileView(QWidget *parent, int x, int y, int width, int height);
    void setImage(QString imagePath);
    void setEmpty();

private:
    QLabel* m_label;
    QString m_style;

};

#endif // FACEUPPILEVIEW_H
