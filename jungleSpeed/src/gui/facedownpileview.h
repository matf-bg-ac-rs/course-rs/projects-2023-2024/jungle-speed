#ifndef FACEDOWNPILEVIEW_H
#define FACEDOWNPILEVIEW_H

#include <QLabel>
#include "facedownpileview.h"

class FaceDownPileView
{
public:
    FaceDownPileView(QWidget *parent, int x, int y, int width, int height);
    void setEmpty();
    void setFull();

private:
    QLabel *m_label;
};

#endif // FACEDOWNPILEVIEW_H
