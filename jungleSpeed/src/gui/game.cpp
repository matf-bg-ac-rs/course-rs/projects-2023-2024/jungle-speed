#include "game.h"
#include "../../forms/ui_game.h"
#include <iostream>
#include <QProgressBar>
#include <QTimer>
#include "../cards/Card.h"

#include "Functions.h"

Game::Game(qsizetype numPlayers, unsigned id, unsigned time, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Game),
    m_numPlayers(numPlayers),
    m_id(id),
    player_id(0),
    m_time(time)

{
    ui->setupUi(this);

    ui->lbIndicator->hide();
    ind = false;

    int totemCards = 78 % numPlayers;
    ui->lbTotemCards->setText(QString::number(totemCards));

    uiPlayers = {ui->player1,ui->player2,ui->player3,ui->player4};
    uiLabels = {ui->pl1Cards, ui->pl2Cards, ui->pl3Cards, ui->pl4Cards};
    uiDiscardLabels = {ui->pl1Discard, ui->pl2Discard, ui->pl3Discard, ui->pl4Discard};
    ui->pbTotem->setDisabled(true);
    timer = new QTimer();
    timer->setSingleShot(true);
    connect(timer, &QTimer::timeout, this, &Game::updateTotem);
    connect(ui->pbTotem, &QPushButton::clicked, this, &Game::pbTotem_clicked);

    setFaceDownCards();
    setFaceUpCards();

    QVector<unsigned> rotated = mapPlayers(m_numPlayers, m_id);
    for (unsigned i = 0; i < numPlayers; i++)
    {
        id_to_pos.insert(rotated[i], i+1);
        pos_to_id.insert(i+1, rotated[i]);
    }

    write_in_chatBox("Welcome to chat!", " ");
    ui->chat->setReadOnly(true);
}

Game::~Game()
{
    delete ui;
}

unsigned Game::getPosFromId(unsigned id)
{
    return id_to_pos[id];
}

unsigned Game::getIdFromPos(unsigned pos)
{
    return pos_to_id[pos];
}

void Game::setUsernames(QList<QString> &usernames)
{
    for(int i = 0; i < usernames.size(); i++)
    {
        uiPlayers[i]->setText(usernames[getIdFromPos(i+1)-1]);
    }
}

void Game::setFaceDownCards()
{
    uiFaceDownCards = {
        new FaceDownPileView(this, 370, 490, 60, 60),
        new FaceDownPileView(this, 50, 270, 60, 60),
        new FaceDownPileView(this, 370, 50, 60, 60),
        new FaceDownPileView(this, 700, 270, 60, 60),
    };
}

void Game::setFaceUpCards()
{
    uiFaceUpCards = {
        new FaceUpPileView(this, 370, 410, 60, 60),
        new FaceUpPileView(this, 150, 270, 60, 60),
        new FaceUpPileView(this, 370, 130, 60, 60),
        new FaceUpPileView(this, 600, 270, 60, 60)
    };
}

void Game::turnCardOver(const unsigned playerId, const QString& color, const QString& shape, const QString& specialType, const int deck_size, const int discard_size)
{
    QString cardImagePath = getPathFromCard(color, shape, specialType);
    uiFaceUpCards[getPosFromId(playerId)-1]->setImage(cardImagePath);

    if (deck_size == 0)
    {
        uiFaceDownCards[getPosFromId(playerId)-1]->setEmpty();
    }
    else
    {
        uiFaceDownCards[getPosFromId(playerId)-1]->setFull();
    }


    uiLabels[getPosFromId(playerId)-1]->setText(QString::number(deck_size));
    uiDiscardLabels[getPosFromId(playerId)-1]->setText(QString::number(discard_size));

    if (specialType == "color")
        ind = !ind;

    if (ind == true)
        ui->lbIndicator->show();
    else
        ui->lbIndicator->hide();

}

void Game::pbTotem_clicked()
{
    ui->pbTotem->setDisabled(true);
    emit sendTotemGrabRequest();
}


void Game::write_in_chatBox(const QString msg, const QString messageType)
{
    if (messageType != "card")
    {
        ui->chat->append(msg);
    }

}
void Game::handleTotemGrabbed(const QString playerMsg)
{
    ui->chat->append(playerMsg);
}

void Game::handleTotemInteractionAllowed()
{
    timer->start(m_time);
    ui->pbTotem->setDisabled(false);
    ui->pbTotem->setStyleSheet("image: url(:/totem/resources/img/totem/totem.png);background: transparent;border: 5px solid green; border-radius:50px");
}


void Game::handleInward(const unsigned id, const QString &name, const int totem_cards)
{
    auto pos = getPosFromId(id)-1;
    uiFaceUpCards[pos]->setEmpty();
    uiDiscardLabels[pos]->setText("0");
    QString msg = QString(name).append(" ").append("won").append(" duel").append(" using inward facing card.");
    ui->chat->append(msg);
    ui->lbTotemCards->setText(QString::number(totem_cards));
}

void Game::updateTotem()
{

    ui->pbTotem->setDisabled(true);
    ui->pbTotem->setStyleSheet("image: url(:/totem/resources/img/totem/totem.png);background: transparent;border: 5px solid red;border-radius:50px");

}

void Game::printDuelResult(const QString &name, const QString &result, const int  id, const int totem_cards)
{
    QString msg = QString(name).append(" ").append(result).append(" duel");
    ui->chat->append(msg);
    ui->lbTotemCards->setText(QString::number(totem_cards));
    if (result=="won") {
        uiFaceUpCards[getPosFromId(id)-1]->setEmpty();
        uiDiscardLabels[getPosFromId(id)-1]->setText("0");
    }
    else {
        for (int i = 1; i <= m_numPlayers; i++)
            if (i != id) {
                uiFaceUpCards[getPosFromId(i)-1]->setEmpty();
                uiDiscardLabels[getPosFromId(i)-1]->setText("0");
            }
    }
}

