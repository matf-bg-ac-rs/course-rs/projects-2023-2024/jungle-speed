#ifndef MUSIC_H
#define MUSIC_H

#include <QtMultimedia/QSoundEffect>

class Music
{
public:
    Music(bool isSoundOn,int value);
    void playMusic();
    void stopMusic();
    bool musicStatus();
    void setMusicVolume(int value);
    unsigned getOldVolume();
    float scaleVolume(int value);
    void setOldVolume(int value);
    void muteVolume();
    float getVolume();
    void changeState();
    ~Music();

public slots:
    void changeMusicVolume();

private:
    bool m_musicOn;
    QSoundEffect *mediaPlayer;
    unsigned m_oldValue;
};

#endif // MUSIC_H
