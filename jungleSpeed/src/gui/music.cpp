#include "music.h"

Music::Music(bool isSoundOn, int value)
{
    this->m_musicOn = isSoundOn;
    mediaPlayer = new QSoundEffect();
    mediaPlayer->setSource(QUrl::fromLocalFile(":/resources/music/pianos-by-jtwayne-7-174717.wav"));
    mediaPlayer->setVolume(value);
    mediaPlayer->setLoopCount(QSoundEffect::Infinite);
    m_oldValue = value;
    if(m_musicOn)
    {
        mediaPlayer->play();
    }
}
void Music::playMusic()
{
    if (!m_musicOn)
    {
        mediaPlayer->play();
        this->m_musicOn = true;
    }
}
void Music::stopMusic()
{
    if (m_musicOn)
    {
        this->m_oldValue = mediaPlayer->volume();
        mediaPlayer->stop();
        this->m_musicOn = false;
    }
}
bool Music::musicStatus()
{
    if (m_musicOn){
        return true;
    }
    else{
        return false;
    }
}
void Music::setMusicVolume(int value){
    mediaPlayer->setVolume(scaleVolume(value));
}

void Music::setOldVolume(int value){

    m_oldValue = scaleVolume(value);
}
unsigned Music::getOldVolume(){

    return m_oldValue;
}
Music::~Music()
{
    delete mediaPlayer;
}

float Music::scaleVolume(int value){
    return static_cast<float>(value) / 100;
}

float Music::getVolume(){
    return mediaPlayer->volume();
}
void Music::muteVolume(){

    mediaPlayer->setMuted(true);
}

void Music::changeState(){

    if(mediaPlayer->isMuted()){
        mediaPlayer->setMuted(false);
        m_musicOn = true;
    }else{
        mediaPlayer->setMuted(true);
        m_musicOn = false;
    }
}
