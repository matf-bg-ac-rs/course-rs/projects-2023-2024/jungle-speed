# Jungle Speed

* Projekat se zasniva na konceptima društvene igre Jungle Speed. Igra se sa nestandardnim kartama i totemom. Idući u smeru kazaljke na satu, igračima se naizmenično otvara po jedna karta iz špila neotvorenih karata i stavlja na vrh špila otvorenih karata. Ako zadovoljavaju kriterijum za ulazak u duel, igrači se takmiče da što pre uhvate totem, čime se oslobađaju određenog dela karata. Pobednik je igrač koji prvi ostane bez karata. Kriterijum za ulazak u duel može biti poređenje po obliku, boji ili izvlačenje specijalne karte "inward facing card".
* Predviđena za 4 igrača.

# Demo snimak

 <a href="https://www.youtube.com/watch?v=HRrUHowAeGs">Jungle Speed</a>

# Jezik i okruženje:
* [![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-red)](https://www.cplusplus.com/) <br>
* [![qt6](https://img.shields.io/badge/Framework-Qt6-blue)](https://doc.qt.io/qt-6/) <br>
* [![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-olivia)](https://www.qt.io/download) <br>

# Instalacija:
* Preuzeti i instalirati [*Qt* i *Qt Creator*](https://www.qt.io/download).
* Qt Multimedia

# Preuzimanje i pokretanje:
1. `$ git clone git@gitlab.com:matf-bg-ac-rs/course-rs/projects-2023-2024/jungle-speed.git`
2. Pokrenuti server. <br>
    2.1. Otvoriti CMakeLists.txt iz foldera server. <br>
    2.2. Izmeniti liniju 10: `set(Qt6_DIR "putanja do Qt")`
3. Pokrenuti 4 instance projekta.
4. Uneti potrebne podatke i započeti igru u svakoj.

# Članovi tima:
 - <a href="https://gitlab.com/minakovandzic">Mina Kovandžić 127/2019</a>
 - <a href="https://gitlab.com/kopdejana">Dejana Kop 91/2019</a>
 - <a href="https://gitlab.com/anjacolic2000">Anja Čolić 231/2019</a>
 - <a href="https://gitlab.com/lazar.kracunovic92">Lazar Kračunović 36/2019</a>
 - <a href="https://gitlab.com/Ogrfil">Filip Ogrenjac 275/2019</a>
 - <a href="https://gitlab.com/misikori">Miloš Obrenović 108/2020</a>


