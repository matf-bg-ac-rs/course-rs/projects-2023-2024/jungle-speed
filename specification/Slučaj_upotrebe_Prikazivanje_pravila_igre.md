# Slučaj upotrebe "Prikazivanje pravila igre"

**Kratak opis:** <br>
Igrač se upoznaje sa pravilima igre i primerima karata.

**Akteri:** <br>
Igrač

**Preduslovi:** <br>
Aplikacija je pokrenuta i korisnik je iz glavnog menija izabrao opciju "Instructions".

**Postuslovi:** <br> /

**Osnovni tok:** <br>
1. Korisnik je kliknuo dugme "Instructions".
2. Otvara se prozor sa opisom igre.
3. Ako je igrač izabrao opciju "How to play": <br>
    3.1. Prikazuje se detaljan opis igranja igre.
4. Ako je igrač izabrao opciju "Special cards": <br>
    4.1. Prikazuje se opis specijalnih karata.
5. Igrač je kliknuo dugme "Main Menu".
6. Povratak na prozor "Main Menu".

**Alternativni tokovi:** / <br>

**Podtokovi:** / <br>
**Specijalni zahtevi:** / <br>
**Dodatne informacije:** / <br>