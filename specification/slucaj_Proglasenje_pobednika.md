# Naziv: "Proglašenje pobednika"

**Kratak opis:**  
Jedan igrač je ostao bez karata. Igra se završava. Otvara se prozor sa prikazom pobednika.

**Akteri:**  
Igrači

**Preduslovi:**  
Jedan igrač se oslobodio svih karata.

**Postuslovi:**  
Povratak na Glavni meni.

**Osnovni tok**
1. Igrač je ostao bez karata. 
2. Aplikacija prikazuje informacije o pobedniku na ekranu.
3. Povratak na Glavni meni.

**Alternativni tokovi:**  
Igrač je neočekivano izašao iz igre.

**Podtokovi:** /  

**Specijalni zahtevi:** /  