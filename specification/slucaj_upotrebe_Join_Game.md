# Naziv: "Join Game"

**Kratak opis:** <br>
Povezivanje igrača na server.

**Akteri:**  
Igrači

**Preduslovi:** 
Aplikacija je pokrenuta i korisnik je kliknuo na prozor Join Game.

**Postuslovi:**
Korisnik je povezan na server.

**Osnovni tok:**

1. Igraču se klikom na dugme "New Game" otvara prozor za unos korisničkog imena.
2. Igrač unosi potrebne podatke i klikne na dugme "Join Game".
3. Aplikacija prosleđuje unete podatke serveru.
4. Igrač čeka da se pridruže ostali igrači da bi se pokrenula igra.
5. Prelazak na slučaj upotrebe "Igranje jedne partije igre".

**Alternativni tokovi:**  
Igrač je neočekivano izašao iz aplikacije.

**Podtokovi:** /  

**Specijalni zahtevi:** /  

**Dodatne informacije:** /