# Naziv: "Duel"

**Kratak opis:** <br>
Izvlačenjem specijalne karte Inward-Facing Card iz špila karata nekog od igrača počinje specijalna vrsta duela. Svi igrači se trkaju da uhvate totem bez obzira na to da li se oblik na njihovoj otvorenoj karti poklapa sa oblikom nekog drugog igrača.

**Akteri:**  
Igrači

**Preduslovi:** 
Neko od igrača je izvukao Inward-Facing kartu iz svog špila.

**Postuslovi:**
Dobijen je pobednik specijalnog duela.

**Osnovni tok:**

1. Nakon što je neko od igrača izvukao Inward-Facing kartu, igrač pokušava da uhvati totem.
2. Aplikacija utvrđuje pobednika duela. Ishod se ispisuje na ekranu.
3. Ukoliko je igrač pobednik duela:

    3.1.  Igrač smešta svoj špil otvorenih karata ispod totema.
    
    3.2. Prelazi se na korak 4.

4. Prelazi se na slučaj upotrebe "Igranje jedne partije igre".

**Alternativni tokovi:**  
Igrač je neočekivano izašao iz igre.

**Podtokovi:** /  

**Specijalni zahtevi:** /  

**Dodatne informacije:** /