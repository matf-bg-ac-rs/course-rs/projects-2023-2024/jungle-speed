# Slučaj upotrebe "Podešavanja"

**Kratak opis:** <br>
Igrač može da podesi jačinu muzike.

**Akteri:** <br>
Igrač

**Preduslovi:** <br>
Aplikacija je pokrenuta. Igrač iz glavnog menija bira opciju "Settings".

**Postuslovi:** <br>
Podešena je jačina muzike. Podešavanja traju sve dok ne dođe do novih izmena ili dok igrač ne izađe iz aplikacije.

**Osnovni tok:** <br>
1. Igrač iz glavnog menija bira opciju "Podešavanje zvuka".
2. Otvara se prozor u kome su opcije za podešavanje jačine muzike.
3. Ako igrač promeni jačinu muzike: <br>
    3.1. Nova jačina muzike se pamti do sledeće izmene ili izlaska iz aplikacije.
4. Ako igrač klikne na mute/unmute dugme: <br>
    5.1. Pamti se do sledeće izmene ili izlaska iz aplikacije.
6. Igrač se vraća na početni meni.

**Alternativni tokovi:** <br>
A1. Neočekivani izlaz iz aplikacije: Gube se podešavanja. <br>

**Podtokovi:** / <br>
**Specijalni zahtevi:** / <br>
**Dodatne informacije:** / <br>