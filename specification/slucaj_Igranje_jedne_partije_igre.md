# Naziv: "Igranje jedne partije igre"

**Kratak opis:**  
Počela je partija. Mogu da igraju 4 igrača. Svaki igrač ima špil neotvorenih i otvorenih karata. Igrači u smeru kazaljke na satu uzimaju po jednu kartu iz špila neotvorenih karata i stavljaju je na vrh špila otvorenih karata. Ukoliko dva ili više igrača imaju otvorenu kartu sa istim oblikom, takmiče se ko će prvi uhvatiti totem. U zavisnosti od ishoda duela i napravljenih grešaka, igrači mogu da se oslobađaju karata ili da dobijaju još karata. Cilj je što pre ostati bez karata.

**Akteri:**  
Igrači

**Preduslovi:**  
Uspostavljena je konekcija između klijenata i servera.

**Postuslovi:**  
Partija je završena i proglašava se pobednik igre.

**Osnovni tok:**
1. Aplikacija inicijalizuje igru.
2. Karte se mešaju i svakom igraču se dodeljuje jednak broj, a preostale karte se postavljaju ispod totema.
3. Sve dok jedan igrač ne ostane bez karata ponavljaju se sledeći koraci:
    - **3.1.** Igrač je na potezu i uzima jednu kartu iz špila neotvorenih karata i stavlja je na vrh svog špila otvorenih karata.
    - **3.2.** Ukoliko je igrač izvukao običnu kartu:
        - **3.2.1.** Prelazi se na korak 3.4.
    - **3.3.** Ukoliko je igrač izvukao specijalnu kartu, primenjuje se njen efekat:
        - **3.3.1.** Ako je igrač izvukao Inward-facing kartu:
            - **3.3.1.1.** Prelazi se na slučaj upotrebe "Duel". Po završetku, prelazi se na korak 3.5.
        - **3.3.2.** Ako je igrač izvukao Outward-facing kartu:
            - **3.3.2.1.** Aplikacija istovremeno otvara po jednu kartu svim igračima. Prelazi se na korak 3.4.
        - **3.3.3.** Ako je igrač izvukao Color kartu:
            - **3.3.3.1.** Karta je aktivna, dokle god se ponovo ne izvuče Color karta. Prelazi se na korak 3.4.
    - **3.4.** Ako igrač pokuša da uhvati totem:
        - **3.4.1.** Aplikacija utvrđuje pobednika duela. Ishod duela se ispisuje na ekranu.
        - **3.4.2.** Ako je igrač pokušao da uhvati totem a ne ispunjava uslove za duel:
            - **3.4.2.1.** Igrač dobija sve karte iz otvorenih špilova svih ostalih igrača kao i karte koje se nalaze ispod totema. U svoj špil zatvorenih karata smešta prikupljene karte kao i karte iz svog špila otvorenih karata.
            - **3.4.2.2.** Prelazi se na korak 3.5.
        - **3.4.3.** Ako je igrač pobednik:
            - **3.4.3.1.** Igrač gubitnicima predaje svoje karte iz otvorenog špila.
            - **3.4.3.2.** Prelazi se na korak 3.5.
        - **3.4.4.** Ako je igrač izgubio duel:
            - **3.4.4.1.** Karte iz špila otvorenih karata pobednika se ravnomerno raspoređuju igračima koji su izgubili duel. Igrač dobija deo ovih karata i smešta ih u svoj špil zatvorenih karata. Preostale karte aplikacija smešta ispod totema. Igrač svoj špil otvorenih karata prebacuje u svoj špil zatvorenih karata.
            - **3.4.4.2.** Prelazi se na korak 3.5.
    - **3.5.** Sledeći igrač je na redu.
4. Aplikacija proglašava pobednika igre. Prelazi se na slučaj upotrebe "Proglašenje pobednika".

**Alternativni tokovi:**  
Igrač je neočekivano izašao iz igre.

**Podtokovi:** /  

**Specijalni zahtevi:** /  

**Dodatne informacije:**  
- **Duel**:
    Ukoliko dva ili više igrača imaju otvorene karte na kojima je isti oblik, takmiče se da uhvate totem. Pobednik je onaj igrač koji prvi pritisne dugme za hvatanje totema. U slučaju da je aktivna Color karta posmatraju se boje umesto oblika.
